angular.module('starter.controllers', [])

.controller('TransCtrl', function($scope, $translate) {
  $scope.changeLanguage = function (key) {
    $translate.use(key);
  };
})

.controller('DashCtrl', function() {
  var pl = new SOAPClientParameters();

SOAPClient.invoke("http://f5it.dscloud.me:8125/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC", "query", pl, true, GetSoapResponse_callBack);

    function GetSoapResponse_callBack(r, soapResponse)
      {
        alert("GetSoapResponse_callBack");

var fullNodeList = soapResponse.getElementsByTagName("resultXml");
var xmlResult = fullNodeList[0].childNodes[0].nodeValue.toString();

console.log(fullNodeList[0].childNodes[0].nodeValue);
alert(fullNodeList[0].childNodes[0].nodeValue);
}
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
